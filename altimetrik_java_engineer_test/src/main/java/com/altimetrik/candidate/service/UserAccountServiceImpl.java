package com.altimetrik.candidate.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.altimetrik.candidate.dto.UserFormDto;
import com.altimetrik.candidate.entity.UserAccountEntity;
import com.altimetrik.candidate.repository.UserAccountRepository;
import com.altimetrik.candidate.converter.UsersFormConverter;
import com.altimetrik.candidate.exception.UsersGenericException;
import com.altimetrik.candidate.exception.UsersNotFoundException;

@Service
public class UserAccountServiceImpl implements UserAccountService {

	@Autowired
	private UserAccountRepository userAccountRepository;

	@Override
	public UserFormDto saveAccount(UserFormDto userFormDto) {
		if (userAccountRepository.findByEmailAddress(userFormDto.getEmailAddress()) != null) {
			throw new UsersGenericException("A user with a similar ID already exists");
		}
		userFormDto.setId(userAccountRepository.save(UsersFormConverter.fromDtoToEntity(userFormDto)).getId());
		return userFormDto;
	}

	@Override
	public List<UserFormDto> fetchAccountList() {
		List<UserAccountEntity> accounts = (List<UserAccountEntity>) userAccountRepository.findAll();
		return accounts.stream().map(UsersFormConverter::fromEntityToDto).collect(Collectors.toList());
	}

	@Override
	public UserFormDto readAccount(Long accountId) {
		Optional<UserAccountEntity> userAccount = userAccountRepository.findById(accountId);
		if (userAccount.isPresent()) {
			return UsersFormConverter.fromEntityToDto(userAccount.get());
		} else {
			throw new UsersNotFoundException("No user account found with id: " + accountId);
		}
	}

	@Override
	public void deleteAccountById(Long accountId) {
		if (userAccountRepository.findById(accountId).isPresent()) {
			userAccountRepository.deleteById(accountId);
		} else {
			throw new UsersNotFoundException("No user account found with id: " + accountId);
		}
	}
}