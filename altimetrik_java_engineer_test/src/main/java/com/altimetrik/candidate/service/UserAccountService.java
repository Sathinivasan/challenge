package com.altimetrik.candidate.service;

import com.altimetrik.candidate.dto.UserFormDto;
import com.altimetrik.candidate.entity.UserAccountEntity;
import java.util.List;
import java.util.Optional;

public interface UserAccountService {

	UserFormDto saveAccount(UserFormDto userFormDto);

	List<UserFormDto> fetchAccountList();

	UserFormDto readAccount(Long accountId);

	void deleteAccountById(Long accountId);
}