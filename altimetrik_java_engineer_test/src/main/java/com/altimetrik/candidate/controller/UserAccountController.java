package com.altimetrik.candidate.controller;

import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.altimetrik.candidate.dto.UserFormDto;
import com.altimetrik.candidate.validator.UserFormValidator;
import com.altimetrik.candidate.service.UserAccountService;

@RestController
@RequestMapping(path = "/api/user")
public class UserAccountController {

	@Autowired
	private UserAccountService userAccountService;

	// To quickly check that these endpoints are reachable over network
	@GetMapping("/ping")
	public String checkAppStatus() {
		return "User api service is Up and Running!";
	}

	// Create user account
	@PostMapping("/account")
	public ResponseEntity<UserFormDto> createUserAccount(
			@Valid @RequestBody UserFormDto userFormDto) {
		UserFormValidator.validateUserFormDto(userFormDto);
		return new ResponseEntity<>(userAccountService.saveAccount(userFormDto), HttpStatus.CREATED);
	}

	// Get list of all accounts
	@GetMapping("/accounts")
	public ResponseEntity<List<UserFormDto>> fetchAccountList() {
		return new ResponseEntity<>(userAccountService.fetchAccountList(), HttpStatus.OK);
	}

	// Read an account by Id
	@GetMapping("/account/{id}")
	public ResponseEntity<UserFormDto> getAccountById(@PathVariable("id") Long accountId) {
		return new ResponseEntity<>(userAccountService.readAccount(accountId), HttpStatus.OK);
	}

	// Remove an account from the list
	@DeleteMapping("/account/{id}")
	public ResponseEntity<String> deleteAccount(@PathVariable("id") Long accountId) {
		userAccountService.deleteAccountById(accountId);
		return new ResponseEntity<>("Deleted Successfully", HttpStatus.ACCEPTED);
	}
}