package com.altimetrik.candidate.repository;

import com.altimetrik.candidate.entity.UserAccountEntity;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserAccountRepository
		extends CrudRepository<UserAccountEntity, Long> {

	UserAccountEntity findByEmailAddress(String email);
}