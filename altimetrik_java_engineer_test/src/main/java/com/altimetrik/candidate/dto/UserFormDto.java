package com.altimetrik.candidate.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class UserFormDto {

	private Long id;
	private String userName;
	private String emailAddress;
	private Double salary;
	private Double expenses;

}
