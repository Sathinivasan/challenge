package com.altimetrik.candidate.validator;

import com.altimetrik.candidate.exception.UsersFunctionalException;
import java.util.ArrayList;
import java.util.List;

import com.altimetrik.candidate.dto.UserFormDto;

public final class UserFormValidator {

	private UserFormValidator() {
	}

	public static void validateUserFormDto(final UserFormDto userFormDto) {
		final List<String> errors = new ArrayList<>();

		// Project type
		if (isBlank(userFormDto.getUserName())) {
			errors.add("User name is mandatory!");
		}
		if (isBlank(userFormDto.getEmailAddress())) {
			errors.add("Users email address is mandatory!");
		}
		if (userFormDto.getSalary() == null || userFormDto.getExpenses() == null) {
			errors.add("Salary and expense details are must!");
		} else {
			if (userFormDto.getSalary() < 1) {
				errors.add("User's salary must be a positive number!");
			}
			if (userFormDto.getExpenses() < 1) {
				errors.add("User's expenses count must be a positive number!");
			}
			if (userFormDto.getSalary() - userFormDto.getExpenses() < 1000) {
				errors.add("User's total credit less than $1000 is not eligible for account creation!!");
			}
		}

		if (!errors.isEmpty()) {
			throw new UsersFunctionalException(errors);
		}
	}

	private static Boolean isBlank(final String input) {
		return input == null || "".equals(input);
	}
}
