package com.altimetrik.candidate.exception;

import java.util.Collections;
import java.util.List;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class UsersExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(value = { UsersFunctionalException.class })
	@ResponseBody
	public ResponseEntity<Object> handlerUsersFunctionalException(final UsersFunctionalException ex) {
		return buildResponseEntity(ex.getErrors(), HttpStatus.PRECONDITION_FAILED.value());
	}

	@ExceptionHandler(value = { UsersNotFoundException.class })
	@ResponseBody
	public ResponseEntity<Object> handlerUsersNotFoundException(final UsersNotFoundException ex) {
		return buildResponseEntity(ex.getErrors(), HttpStatus.NOT_FOUND.value());
	}

	@ExceptionHandler(value = { UsersGenericException.class })
	@ResponseBody
	public ResponseEntity<Object> handlerUsersGenericException(final UsersGenericException ex) {
		return buildResponseEntity(ex.getErrors(), HttpStatus.NOT_ACCEPTABLE.value());
	}

	@ExceptionHandler(value = { DataAccessException.class })
	@ResponseBody
	public ResponseEntity<Object> handlerDataAccessException(final DataAccessException ex) {
		return buildResponseEntity(Collections.singletonList(ex.getLocalizedMessage()),
				HttpStatus.INTERNAL_SERVER_ERROR.value());
	}

	/**
	 * Generic exception handler
	 */
	@ExceptionHandler({ Exception.class })
	@ResponseBody
	public ResponseEntity<Object> handleAnyException(final Exception ex) {
		return buildResponseEntity(Collections.singletonList(ex.getLocalizedMessage()),
				HttpStatus.INTERNAL_SERVER_ERROR.value());
	}

	private ResponseEntity<Object> buildResponseEntity(final List<String> errors, final int httpStatusCode) {
		if (!errors.isEmpty()) {
			errors.forEach(logger::error);
		}
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
		return ResponseEntity.status(httpStatusCode).headers(headers).body(errors);
	}

}
