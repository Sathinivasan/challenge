package com.altimetrik.candidate.exception;

import java.util.Collections;
import java.util.List;

public class UsersGenericException  extends RuntimeException {

	private List<String> errors;

	public UsersGenericException(String error) {
		this(Collections.singletonList(error));
	}

	public UsersGenericException(List<String> errors) {
		this.errors = errors;
	}

	public List<String> getErrors() {
		return errors;
	}

	public void setErrors(List<String> errors) {
		this.errors = errors;
	}

	@Override
	public String getMessage() {
		if (errors == null || errors.isEmpty()) {
			return super.getMessage();
		}
		return String.join(";", errors);
	}


}
