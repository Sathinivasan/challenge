package com.altimetrik.candidate.exception;

import java.util.List;

public class UsersFunctionalException extends UsersGenericException {

	public UsersFunctionalException(String error) {
		super(error);
	}

	public UsersFunctionalException(List<String> errors) {
		super(errors);
	}

}
