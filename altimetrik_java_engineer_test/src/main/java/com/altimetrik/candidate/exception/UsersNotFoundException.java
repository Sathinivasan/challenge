package com.altimetrik.candidate.exception;

import java.util.List;

public class UsersNotFoundException extends UsersGenericException {

	public UsersNotFoundException(String error) {
		super(error);
	}

	public UsersNotFoundException(List<String> errors) {
		super(errors);
	}

}
