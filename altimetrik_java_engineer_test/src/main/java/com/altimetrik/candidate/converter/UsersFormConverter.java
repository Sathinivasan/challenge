package com.altimetrik.candidate.converter;

import com.altimetrik.candidate.dto.UserFormDto;
import com.altimetrik.candidate.entity.UserAccountEntity;

public final class UsersFormConverter {

	private UsersFormConverter() {
	}

	public static UserFormDto fromEntityToDto(UserAccountEntity userAccountEntity) {
		UserFormDto userFormDto = new UserFormDto();
		userFormDto.setId(userAccountEntity.getId());
		userFormDto.setUserName(userAccountEntity.getUserName());
		userFormDto.setEmailAddress(userAccountEntity.getEmailAddress());
		userFormDto.setSalary(userAccountEntity.getSalary());
		userFormDto.setExpenses(userAccountEntity.getExpenses());

		return userFormDto;
	}

	public static UserAccountEntity fromDtoToEntity(UserFormDto userFormDto) {
		UserAccountEntity accountEntity = new UserAccountEntity();
		accountEntity.setUserName(userFormDto.getUserName());
		accountEntity.setEmailAddress(userFormDto.getEmailAddress());
		accountEntity.setSalary(userFormDto.getSalary());
		accountEntity.setExpenses(userFormDto.getExpenses());
		accountEntity.setCreditAmount(userFormDto.getSalary() - userFormDto.getExpenses());
		return accountEntity;
	}
}
