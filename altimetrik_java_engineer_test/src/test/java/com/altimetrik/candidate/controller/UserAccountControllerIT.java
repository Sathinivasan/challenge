package com.altimetrik.candidate.controller;

import com.altimetrik.candidate.entity.UserAccountEntity;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.altimetrik.candidate.UserApiAppTest;
import com.altimetrik.candidate.dto.UserFormDto;
import com.altimetrik.candidate.exception.UsersFunctionalException;
import com.altimetrik.candidate.repository.UserAccountRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = UserApiAppTest.class)
public class UserAccountControllerIT {

	private MockMvc mockMvc;

	@Autowired
	private UserAccountRepository userRepository;

	@Before
	public void setup() {
		this.mockMvc = MockMvcBuilders.standaloneSetup(UserAccountController.class).build();
	}

	@Test
	public void shouldThrowFunctionalException() throws Exception {
		UserFormDto userForm = new UserFormDto();
		userForm.setSalary(0.0);
		userForm.setExpenses(0.0);
		try {
			mockMvc.perform(post("/api/user/account").contentType(MediaType.APPLICATION_JSON)
				.content(new ObjectMapper().writeValueAsString(userForm))).andExpect(status().isOk()).andReturn();
		} catch (Exception exp) {
			assertNotNull(exp.getCause());
			assertTrue(exp.getCause() instanceof UsersFunctionalException);
			assertTrue(exp.getCause().getMessage().contains("User name is mandatory!"));
			assertTrue(exp.getCause().getMessage().contains("Users email address is mandatory!"));
			assertTrue(exp.getCause().getMessage().contains("User's salary must be a positive number!"));
			assertTrue(exp.getCause().getMessage().contains("User's expenses count must be a positive number!"));
			assertTrue(exp.getCause().getMessage().contains("User's total credit less than $1000 is not eligible for account creation!!"));
		}
	}

	@Ignore
	@Test
	public void shouldCreateUserAccount() throws Exception {
		UserFormDto userForm = new UserFormDto();
		userForm.setUserName("TestUser");
		userForm.setEmailAddress("user@test.com");
		userForm.setSalary(15000.0);
		userForm.setExpenses(12000.0);
		mockMvc.perform(post("/api/user/account").contentType(MediaType.APPLICATION_JSON)
				.content(new ObjectMapper().writeValueAsString(userForm))).andExpect(status().isOk()).andReturn();

		final UserAccountEntity userAccount = userRepository.findByEmailAddress("user@test.com");
		assertNotNull(userAccount);
		assertTrue(userAccount.getUserName().equalsIgnoreCase("TestUser"));
	}
}
