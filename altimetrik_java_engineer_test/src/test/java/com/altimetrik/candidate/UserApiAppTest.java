package com.altimetrik.candidate;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableAutoConfiguration
@EnableJpaRepositories(basePackages = { "com.altimetrik.candidate.repository" })
@ComponentScan(basePackages = { "com.altimetrik.candidate" })
@EntityScan(basePackages = { "com.altimetrik.candidate.entity" })
public class UserApiAppTest {

}
