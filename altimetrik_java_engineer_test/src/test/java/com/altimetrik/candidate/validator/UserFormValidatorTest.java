package com.altimetrik.candidate.validator;

import java.util.List;
import org.junit.Test;

import com.altimetrik.candidate.dto.UserFormDto;
import com.altimetrik.candidate.exception.UsersFunctionalException;

import static org.junit.Assert.assertEquals;

public class UserFormValidatorTest {

	@Test
	public void expectErrorsFromValidator() {
		try {
			UserFormDto userForm = new UserFormDto();
			userForm.setSalary(0.0);
			userForm.setExpenses(0.0);
			UserFormValidator.validateUserFormDto(userForm);
		} catch (UsersFunctionalException exp) {
			final List<String> errors = exp.getErrors();
			assertEquals(errors.size(), 5);
			assertEquals(errors.get(0), "User name is mandatory!");
			assertEquals(errors.get(1), "Users email address is mandatory!");
			assertEquals(errors.get(2), "User's salary must be a positive number!");
			assertEquals(errors.get(3), "User's expenses count must be a positive number!");
			assertEquals(errors.get(4), "User's total credit less than $1000 is not eligible for account creation!!");
		}
	}
}
