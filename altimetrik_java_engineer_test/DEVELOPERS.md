# User API Dev Guide
------------------------------------
API_1: Create user account
Request URL: POST /api/user/account
Body: 
	{
		"userName": "TestUser",
		"emailAddress": "user@test.com",
		"salary": 5500,
		"expenses": 3500
	}
Response Header: 201 CREATED
------------------------------------
API_2: Get user accounts
Request URL: GET /api/user/accounts
Response Header: 200 OK
------------------------------------
API_3: Get user account by ID
Request URL: GET /api/user/account/{id}
Response Header: 200 OK
------------------------------------
API_4: Delete user account by ID
Request URL: DELETE /api/user/account/{id}
Response Header: 202 ACCEPTED
------------------------------------

## Building
$ mvn clean install

## Testing
$ mvn test

## Deploying/ Run
$ mvn spring-boot:run

## Additional Information
This application works with the in-memory database h2. The data is reset every time the application is restarted. We can connect to the database with the url /h2-console on the running port with the credentials configured in the properties file.
